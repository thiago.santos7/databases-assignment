const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/test', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => console.log('Database connection was successful!'))

const db = mongoose.connection;

module.exports = db