const mongoose = require('mongoose')
const db = require('./db')

const carSchema = new mongoose.Schema({
    model: String,
    year: String,
    color: String,
    power: Number
});

const Car = db.model('Car', carSchema);

async function createCar(carInfo) {
    console.log(`\nCreating ${carInfo.model} car...`)

    const car = new Car(carInfo)

    return await car.save();
}

async function createManyCars(carsInfo) {
    console.log(`\nCreating ${carsInfo.reduce((acc, current) => acc + ` ${current.model},`, '')} cars...`)

    const cars = await Car.insertMany(carsInfo)
    return cars;
}

async function updateCar(carInfo) {
    console.log(`\nUpdating ${carInfo.model} car...`)

    return await Car.findOneAndUpdate({ model: carInfo.model }, carInfo)
}

function getAllCars() {
    console.log('\nFinding all cars...');

    return Car.find({});
}

async function clearAllCars() {
    console.log('\nClearing all cars from database...')
    return await Car.deleteMany({})
}

(async () => {
    await clearAllCars();
    await createCar({ model: 'Model 3', year: '2020', color: 'blue', power: 200 });
    await updateCar({ model: 'Model 3', power: 300 });
    await createManyCars([{ model: 'Gol' }, { model: 'Corsa' }, { model: 'M5' }]);
    const cars = await getAllCars();
    console.log(JSON.stringify(cars, null, 4));
    console.log('\nDone.')
})();

